import { useRef } from "react";

type constructorCallback = () => void;
export default function useContructor(callback : constructorCallback) : void {
    const isref = useRef(false);

    if(isref.current === false) {
        callback();
        isref.current = true;
    }
    

}