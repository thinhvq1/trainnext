import React, { useEffect, useRef, useState } from 'react'
import useContructor from "../../customhook/useContructor"
let isLogin = false
const Admin = () => {

    const [count, setCount] = useState(0)
    let isContruc = useRef(false);
    useEffect(() => {
        console.log("show admin")
        return () => {
            console.log("remove admin")
        }
    }, [])

    useContructor(() => {
        console.log("use_contructor")
    })
    if(isContruc.current === false) {
        console.log("constructor cach2");
        isContruc.current = true;
    }
    if(isLogin === false){
        console.log("day là cóntrucss");
        isLogin= true
    }
    return (
        <>
            <button onClick={() => {
                setCount((pre) => {
                    return pre +1
                })
            }}>CLick</button>
            <p>{count}</p>
        </>
    )
}
export default  Admin;