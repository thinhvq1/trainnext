import '../styles/globals.css'
import "../styles/test.scss"
import {AppProps} from "next/app"
import { Component } from 'react'

// export default class MyApp extends Component<AppProps>{
//   render() {
//     return <this.props.Component {...this.props.pageProps} />
//   }
// }
// const MyApp : React.FC<AppProps> = ({ Component, pageProps }) => {
//   return <Component {...pageProps} />
// }

// export default MyApp
function MyApp ({ Component, pageProps })  {
  return <Component {...pageProps} />
}

export default MyApp
